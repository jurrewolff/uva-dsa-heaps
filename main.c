/* Name: Jurre Wolff
 * Student ID: 14085100
 * Course: Datastructuren
 * Institution: University of Amsterdam
 *
 * main.c:
 * DESCRIPION:
 *    Entry point of program execution.
 *
 *    The program reads input line by line from stdin. Every line represents
 *    patient information (name, age, treatment duration). Based on a priority
 *    queue sorted on initials and on age (if -y flag is passed) the program
 *    will "treat" patients, printing their names and a "." after treatment. All
 *    patients that have not yet been treated after a full "working day" of 10
 *    hours (iterations), are printed sequentially.
 *
 * USAGE:
 *    "./queue" will run the program and stdin will be used as input until EOF
 *    is reached. Subsequent patient info represent new patients walking in at
 *    the same time. A "." signals the end of an hour where patients might've
 *    come in. In total 10 "." must be present to represent 10 hours.
 *
 *    -y: Sort on priority queue based on patient age.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "prioq.h"

#define BUF_SIZE 1024

static char buf[BUF_SIZE];
char delims[] = " \n";

struct config {
  /* Set to 1 if -y is specified, 0 otherwise. */
  int year;
};

static int parse_options(struct config *cfg, int argc, char *argv[]);

typedef struct {
  char *name;
  int age;
  int duration;
  int treated_time;
} patient_t;

static int compare_patient_name(const void *a, const void *b) {
  return strcmp(((const patient_t *)a)->name, ((const patient_t *)b)->name);
}

// Compare patient ages.
//
// a: Patient object to compare with b.
// b: Patient object to compare with a.
//
// Returns: Positive int if patient a > b or negative int if a < b or the result
//          of compare_patient_name() if bot a and b are equal.
static int compare_patient_age(const void *a, const void *b) {
  int age_a = ((const patient_t *)a)->age;
  int age_b = ((const patient_t *)b)->age;

  if (age_a == age_b) {
    return compare_patient_name(a, b);
  } else {
    return age_a - age_b;
  }
}

// Parse line into patient structure.
//
// line: the line containing information about a patient.
// delims: The delimiters used to split information on line apart.
//
// Returns: An initialized patient struct with default values in case of missing
//          information.
patient_t *get_patient_from_line(char *line, char *delims) {
  patient_t *new_patient = malloc(sizeof(patient_t));
  if (!new_patient) {
    return NULL;
  }

  // Default member values.
  new_patient->duration = 1;
  new_patient->treated_time = 0;

  char *token = strtok(line, delims);
  int count = 0;
  while (token) {
    if (strcmp(token, ".") == 0) {
      free(new_patient);
      return NULL;
    }

    if (count == 0) {
      new_patient->name = malloc(sizeof(char) * (strlen(token) + 1));
      if (!new_patient->name) {
        free(new_patient);
        return NULL;
      }
      strcpy(new_patient->name, token);

    } else if (count == 1) {
      char *end_ptr;
      int age = (int)strtol(token, &end_ptr, 10);
      new_patient->age = age;

    } else if (count == 2) {
      char *end_ptr;
      int duration = (int)strtol(token, &end_ptr, 10);
      new_patient->duration = duration;

    } else {
      free(new_patient);
      return NULL; // Unforeseen situation.
    }

    count++;
    token = strtok(NULL, delims);
  }

  if (count == 0) {
    free(new_patient);
    return NULL;
  }

  return new_patient;
}

// Clean patient_t and its members from memory.
//
// p: Pointer to patient_t object.
void free_func(void *p) {
  free(((patient_t *)p)->name);
  free(p);
}

// Parse line present on stdin.
//
// Returns: Character array containing single line as parsed from stdin.
char *parse_line(void) {
  char *s = fgets(buf, BUF_SIZE, stdin);
  if (s == NULL) {
    fprintf(stderr, "Unexpected end of file. exiting\n");
    return NULL;
  }

  char *line = calloc(BUF_SIZE, sizeof(char));
  if (!line) {
    return NULL;
  }
  strcpy(line, buf);

  return line;
}

// Get patients from stdin and store them in priority queue.
//
// queue: The priority queue to store the patients in.
//
// Returns: 0 on success or 1 on failure.
int let_patients_in(struct heap *queue) {
  while (1) {
    char *line = parse_line();
    if (!line) {
      prioq_cleanup(queue, free_func);
      return 1;
    }

    patient_t *new_patient = get_patient_from_line(line, delims);
    if (new_patient) {
      if (prioq_insert(queue, new_patient) != 0) {
        return 1;
      }
    } else {
      free(line);
      break;
    }
    free(line);
  }

  return 0;
}

// Treat next patient in queue.
//
// queue: The queue to get next patient from.
// occupying_patient: A patient that might currently be occupying the doctor.
//
// Returns: An occupying patient in case treatment takes longer than 1 session.
patient_t *treat_next_patient(struct heap *queue,
                              patient_t *occupying_patient) {
  if (occupying_patient) {
    if (++occupying_patient->treated_time == occupying_patient->duration - 1) {
      printf("%s\n", occupying_patient->name);

      free_func(occupying_patient);
      occupying_patient = NULL;
    }

  } else {
    patient_t *patient_to_treat = prioq_pop(queue);
    if (patient_to_treat && patient_to_treat->duration > 1) {
      occupying_patient = patient_to_treat;

    } else if (patient_to_treat) {
      printf("%s\n", patient_to_treat->name);
      free_func(patient_to_treat);
    }
  }

  return occupying_patient;
}

// Make all untreated patient present in queue leave.
//
// queue: The queue to get untreated patients from.
// occupying_patient: The patient that might currently be occupying the doctor.
void make_patients_leave(struct heap *queue, patient_t *occupying_patient) {
  patient_t *untreated_patient = prioq_pop(queue);

  if (occupying_patient) {
    printf("%s\n", occupying_patient->name);
    free_func(occupying_patient);
  }

  while (untreated_patient != NULL) {
    printf("%s\n", untreated_patient->name);

    free_func(untreated_patient);
    untreated_patient = prioq_pop(queue);
  }
}

int main(int argc, char *argv[]) {
  prioq *queue;
  struct config cfg;

  if (parse_options(&cfg, argc, argv) != 0) {
    return EXIT_FAILURE;
  }

  if (cfg.year) {
    queue = prioq_init(&compare_patient_age);
  } else {
    queue = prioq_init(&compare_patient_name);
  }

  patient_t *occupying_patient = NULL;
  for (int iterations = 0;;) {
    if (let_patients_in(queue) != 0) {
      return EXIT_FAILURE;
    }

    occupying_patient = treat_next_patient(queue, occupying_patient);

    printf(".\n"); // Time passed

    if (++iterations == 10) {
      make_patients_leave(queue, occupying_patient);
      break;
    }
  }

  prioq_cleanup(queue, free_func);
}

int parse_options(struct config *cfg, int argc, char *argv[]) {
  memset(cfg, 0, sizeof(struct config));
  int c;
  while ((c = getopt(argc, argv, "y")) != -1) {
    switch (c) {
    case 'y':
      cfg->year = 1;
      break;
    default:
      fprintf(stderr, "invalid option: -%c\n", optopt);
      return 1;
    }
  }
  return 0;
}
