/* Name: Jurre Wolff
 * Student ID: 14085100
 * Course: Datastructuren
 * Institution: University of Amsterdam
 *
 * heap.c:
 * DESCRIPION:
 *   Definitions of heap DS functions for creating, cleaning, inserting and
 *   getting.
 */

#include <stdlib.h>

#include "array.h"
#include "prioq.h"

// Recursively percolate heap nodes up, starting at provided index and stopping
// at heap root.
//
// h: The heap to work with.
// index: The index to start percolating upwards at.
static void percolate_up(struct heap *h, long index) {
  double decimal_parent = (double)(index - 1) / 2;
  if (decimal_parent < 0) {
    return; // Negative parent index signals we're at root.
  }

  long parent = (long)decimal_parent;
  long priority = index;

  if (parent >= 0 &&
      h->compare(array_get(h->array, index), array_get(h->array, parent)) > 0) {
    priority = parent;
  }

  if (parent >= 0 && priority == index) {
    void *temp_parent = array_get(h->array, parent);
    array_set(h->array, parent, array_get(h->array, index));
    array_set(h->array, index, temp_parent);

    percolate_up(h, parent);
  }
}

// Recursively percolate heap nodes down, starting at provided index and
// stopping at leaf node.
//
// h: The heap to work with.
// index: The index to start percolating downwards at.
static void percolate_down(struct heap *h, long index) {
  long left_child = (index * 2) + 1;
  long right_child = (index * 2) + 2;
  long priority = index;

  if (left_child < array_size(h->array) && right_child < array_size(h->array) &&
      h->compare(array_get(h->array, left_child),
                 array_get(h->array, right_child)) > 0) {
    priority = right_child;
  } else if (left_child < array_size(h->array)) {
    priority = left_child;
  }

  if (h->compare(array_get(h->array, priority), array_get(h->array, index)) >
      0) {
    priority = index;
  }

  if (priority != index) {
    void *temp_larger_child = array_get(h->array, priority);
    array_set(h->array, priority, array_get(h->array, index));
    array_set(h->array, index, temp_larger_child);

    percolate_down(h, priority);
  }
}

// Create heap DS with members initialized.
//
// compare: The comparison function used in heap to satisfy heap property.
//
// Returns: Heap structure on success or NULL on failure.
static struct heap *heap_init(int (*compare)(const void *, const void *)) {
  if (!compare) {
    return NULL;
  }

  struct heap *h = malloc(sizeof(struct heap));
  if (!h) {
    return NULL;
  }

  h->array = array_init(16);
  if (!h->array) {
    free(h);
    return NULL;
  }

  h->compare = compare;

  return h;
}

prioq *prioq_init(int (*compare)(const void *, const void *)) {
  return heap_init(compare);
}

long int prioq_size(prioq *q) { return array_size(q->array); }

// Cleanup heap DS and its members from memory.
//
// h: The heap to clean.
// free_func: The function used to free heap array elements.
//
// Returns: 0 on success or 1 on failure.
static int heap_cleanup(struct heap *h, void free_func(void *)) {
  if (!free_func) {
    free_func = free;
  }

  array_cleanup(h->array, free_func);
  free(h);

  return 0;
}

int prioq_cleanup(prioq *q, void free_func(void *)) {
  return heap_cleanup(q, free_func);
}

// Insert element into heap.
//
// h: The heap to insert element into.
// p: The element object to insert.
//
// Returns: 0 on success or 1 on failure.
static int heap_insert(struct heap *h, void *p) {
  if (!h) {
    return 1;
  }

  if (array_append(h->array, p) != 0) {
    return 1;
  }

  long arr_size = array_size(h->array);
  if (arr_size > 1) {
    percolate_up(h, arr_size - 1);
  }

  return 0;
}

int prioq_insert(prioq *q, void *p) { return heap_insert(q, p); }

// Remove element from heap array and return its value.
//
// h: The heap to pop element from.
//
// Returns: Element object on success or NULL on failure.
static void *heap_pop(struct heap *h) {
  void *root = array_get(h->array, 0);
  void *last_elem = array_pop(h->array);
  if (!last_elem) {
    return NULL;
  }

  if (root == last_elem) {
    return root;
  }

  if (array_set(h->array, 0, last_elem) != 0) {
    return NULL;
  }

  percolate_down(h, 0);

  return root;
}

void *prioq_pop(prioq *q) { return heap_pop(q); }
